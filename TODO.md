# TODO

Here is a list of tasks to do (moved to [CHANGELOG.md](CHANGELOG.md) once done). 

Related TODO files:
- [glimmer-dsl-swt/TODO.md](https://github.com/AndyObtiva/glimmer-dsl-swt/blob/master/TODO.md)

## Tasks

### Version TBD

- Consider creating a configuration DSL
- Rename Observable methods (including subclasses) to observe/unobserve
- Prefix ObservableModel utility methods with double-underscore
